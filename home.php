<!DOCTYPE html>
<html>
<head>
    <title>Progetto SM</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="style.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="plugins/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="http://d3js.org/d3.v4.min.js"></script>
    <script type="text/javascript" src="graphic1.js"></script>
    <script type="text/javascript" src="graphic2.js"></script>
    <script type="text/javascript" src="graphic3.js"></script>
    <script type="text/javascript" src="graphic_manager.js"></script>
</head>

<body>

<div class="container-fluid">
  <div class="row title">
    <div class="col-sm-12">
      <h1>Progetto Sistemi Multimediali 2016-17</h1>
    </div>
  </div>
  <div class="row nav">
        <div class="col-sm-4">
          <button type="button" class="graphic-btn" id="1">Pie Chart</button>
        </div>
        <div class="col-sm-4">
            <button type="button" class="graphic-btn" id="2">Horizontal Bar-Chart</button>
        </div>
        <div class="col-sm-4">
          <button type="button" class="graphic-btn" id="3">Collapsible Tree</button>
        </div>
    </div>

    <div class="row sections">
      <div id="section1" class="col-sm-12 graphic">
        <h2>Pie Chart</h2>
        <p>At the beginning, you can see a Pie Chart containing all actions made across all the years, in percentage.
          A legend on the right will show you to which action correspond each color.
          Hovering over the pie, makes a tooltip appear.
          The tooltip shows: the name of the action you are hovering on,
          how many actions of that type were made (counter),
          the percentage of the action counter according to the pie in its totality,
          the acres affected by all the actions of that type.
          You can choose an option from the select to have more specific information about only one year.
        </p>
        <select id="select_g1" name="years">
        <option value="default" selected="true">Choose a year</option>
        </select><br>
        <div id="chart"></div>
      </div>
      <div id="section2" class="col-sm-12 graphic">
        <h2>Horizontal Bar-Chart</h2>
        <p>At the beginning, there's only a blank page with two select.
          After selecting an element from one of the two select, an Horizontal Bar-Chart will appear showing the actions made by it.
          A legend on the right will show you by who were made those actions.
          You can choose a second element to make a comparison of the actions made by both.
          The X axis represents the action counter (i.e. how many actions of a certain type were made).
          The Y axis represents the action names.
          Hovering over the bars, makes a tooltip appear.
          The tooltip shows: the name of the action you are hovering on,
          how many actions of that type were made (counter),
          the acres affected by all the actions of that type.
        </p>
        <select id="select_g2_first" class="select_g2" name="president-congress">
        <option value="default" selected="true">Choose a president or a congress</option>
      </select>
      <select id="select_g2_second"class="select_g2" name="president-congress">
      <option value="default" selected="true">Choose a president or a congress</option>
    </select><br>
      </div>
      <div id="section3" class="col-sm-12 graphic">
        <h2>Collapsible Tree</h2>
        <p>At the beginning, you can see a collapsible tree with root node (called "States") containing all the States of America that are in the database.
          The child nodes show two piece of information: the State's name and how many parks were subject of some actions across the years in that State.
          After selecting a child node, you will see all the Parks' name.
          Selecting a park will show you some info: when the Park was established, by who was established, and which is its agency.
        </p>
      </div>
    </div>
  </div>

<footer class="container">
  <p>Project made by Pivato Francesco and Roselli Leonardo ©</p>
</footer>

</body>

</html>
