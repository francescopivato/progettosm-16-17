$(document).ready(function(){

    $.getJSON("actions_under_antiquities_act.json", function(data, status){

      var width = 960,
          height = 500,
          prescong = [],
          actions = [],
          i,
          j = 0,
          condition = false
          presidentsTable = [],
          margin = {top: 20, right: 30, bottom: 40, left: 40},
          radius = 16,
          xDomain = [0, 30],
          yDomain = [],
          paddingLeft = 40
          legendHeight = 50,
          id1=1,
          id2=2,
          color1 = "#1F78B4";
          color2 = "#33A02C";

      var svg = d3.select("#section2")
                                  .append("svg")
                                  .attr('width', (width + margin.right + margin.left))
                                  .attr('height', (height + margin.top + margin.bottom + legendHeight))
                                  .attr("class", "svg_panel")
                                  .append('g')
                                  .attr('id', 'svg')
                                  .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

      for(i=0; i<data.length; i++){
        //collecting all presidents or congresses, listed in data, inside prescong[]
        if($.inArray(data[i].pres_or_congress, prescong)==-1){
          prescong[j] = data[i].pres_or_congress;
          j++;
        }
      }

      orderPresidentsAndCongresses();

      initializeActions();

      //adding to the HTML page all the years as an <option> in a <select>
      for (var i = 0; i < prescong.length; i++){
            $(".select_g2").append("<option value=\"\">" + prescong[i] + "</option>");
      }

      //Check if president or congress chosen is tracked by jQuery adding a triggerHandler event
      //to <select>
      // trigger vs triggerHandler
      // trigger: it runs since the default action the element
      //triggerHandler: it avoids the default action of the element

      $("#select_g2_first").select(function(){
        //the if is necessary because everytime I click on the select it shows up the value, so every value
        //would be displayed twice if it wasn't for the if
        if (condition == false){
          condition = true;
        } else {
          // I create the bar chart for the selected element
          makeBarChart($("#select_g2_first option:selected").text(), id1);
          //A small lengend appears on the right
          legendElement(svg, color1, $("#select_g2_first option:selected").text(), width - 200, id1);
          condition=false;
        }
      });

      $("#select_g2_first").click(function(){
        $("#select_g2_first").triggerHandler("select");
      });

      $("#select_g2_second").select(function(){
        //the if is necessary because everytime I click on the select it shows up the value, so every value
        //would be displayed twice if it wasn't for the if
        if (condition == false){
          condition = true;
        } else {
          // I create the bar chart for the selected element
          makeBarChart($("#select_g2_second option:selected").text(), id2);
          //A small lengend appears on the right
          legendElement(svg, color2, $("#select_g2_second option:selected").text(), width - 100, id2);
          condition=false;
        }
      });

      $("#select_g2_second").click(function(){
        $("#select_g2_second").triggerHandler("select");
      });

         var colorScheme = d3.scaleOrdinal().range(d3.schemeCategory10);

         var tooltip = d3.select("body").append("div").attr("class", "toolTip").style("opacity", 0);

         function makeBarChart(selectedElement, id){

           //every time makeBarChart is called I remove all html elements nested in '#svg' except from '.legend'
           $('#svg').find('*').not('.legend').remove();

           //I collect inside the array of objects 'actions[]' all the action done by the selected element
           //Actions are 17. Elements usually do from 1 to 6 actions so most of them will be set to 0.
           getActions(selectedElement, id);

           yDomain = [];

           var actionsDone = [];

           for (var i = 0; i < actions.length; i++) {

             if (actions[i].action_number_1 != 0 || actions[i].action_number_2 != 0) {
               // yDomain changes according to which actions were done by selected options
               //Ex: if none of the selected elements did "Diminished", such action won't be included in yDomain
               //ex: if just one or both selected elements did "Established", such action will be included in yDomain
               yDomain.push(actions[i].action_name);
               //As I said before most of the actions are set to 0 because the element didn't do them
               //In actionsDone[] I collect only the actions that were made by one or both of the selected elements
               actionsDone.push(actions[i]);
             }
           }

           var xScale = d3.scaleLinear()
               .domain(xDomain) //it's set to [0,30] because the most repeated action is repeated 29 times
               .range([0, width - margin.left - margin.right]);

           var yScale = d3.scaleBand()
               .paddingInner(0.5)
               .rangeRound([height, 0])
               .domain(yDomain); //as demonstrated above, yDomain changes dinamically

           var xAxis = d3.axisBottom(xScale);
           var yAxis = d3.axisLeft(yScale);

           //'.drawArea' will be the container for the classes '.barZone'
           var drawArea = svg.append('g')
               .attr('class', 'draw-area')
               .attr('width', width)
               .attr('height', height)
               .attr('transform', 'translate(50,' + legendHeight + ')');

           // setting labels for x and y Axis
           svg.append("text")
               .attr("class", "yAxisLabel")
               .attr("x", 0)
               .attr("y", 0)
               .attr("fill", "#000")
               .attr("text-anchor", "start")
               .text("Type of actions")
               .attr('transform', 'translate(-28,' + ((height-20)) + ')rotate(-90)');
           svg.append("text")
               .attr("class", "xAxisLabel")
               .attr("x", 90)
               .attr("y", legendHeight - 5)
               .attr("fill", "#000")
               .attr("text-anchor", "start")
               .text("Number of actions")
               .attr('transform', 'translate(0,0)');

               //a barZone is created for each action that was done
               //each barZone contains:
               //   - a label of the action
               //   - one or both bars (if one selected element or both did that action)
               //   - one or both action counter labels (if one selected element or both did that action)
           var barZone = drawArea.selectAll(".barZone")
               .data(actionsDone)
               .enter().append("g")
               .attr("class", "barZone");

           // adding action labels to barZone
           // i.e. "Established", "Diminished", ecc. according to which action was made by the selected option
           barZone.append("text")
               .attr("class", "label")
               .attr("x", paddingLeft / 2)
               .attr("y", function (d) {
                   return yScale(d.action_name);
               })
               .attr("dy", "2rem") // change labels position
               .attr("fill", "#000")
               .attr("text-anchor", "end")
               .text(function (d) {
                   return d.action_name;
               });

           //adding the first bar to the barZone (if present)
           barZone.append("rect")
               .attr("class", "bar")
               .attr("x", paddingLeft)
               .attr("height", 20)
               .attr("y", function (d, i) {
                   return yScale(d.action_name);
               })
               .attr("width", function (d) {
                   return xScale(d.action_number_1);
               })
               .attr("fill", color1)
               .on("mousemove", function(d){
                   tooltip
                       .style("left", d3.event.pageX + "px")
                       .style("top", d3.event.pageY - 80 + "px")
                       .style("display", "inline-block")
                       .html(fillTooltip(d.action_name,d.action_number_1,d.acres_affected_1));
               })
               .on("mouseover", function(d){
                 tooltip.transition().duration(200).style("opacity", 0.9);
               })
               .on("mouseout", function(d){
                 tooltip.transition().duration(500).style("opacity", 0);
               });
           //adding the first action number label to the barZone (if present)
           barZone.append("text")
               .attr("x", function (d) {
                   return xScale(d.action_number_1) + paddingLeft;
               })
               .attr("y", function (d,i) {
                   return yScale(d.action_name);
               })
               .attr("dy", "1.1rem")
               .attr("dx", "-0.2rem")
               .attr("fill", "#fff")
               .attr("text-anchor", "end")
               .text(function (d) {
                   return d.action_number_1;
               });

            //adding the second bar to the barZone (if present)
           barZone.append("rect")
               .attr("class", "bar")
               .attr("x", paddingLeft)
               .attr("height", 20)
               .attr("y", function (d, i) {
                   return yScale(d.action_name)+25;
               })
               .attr("width", function (d) {
                   return xScale(d.action_number_2);
               })
               .attr("fill", color2)
               .on("mousemove", function(d){
                   tooltip
                       .style("left", d3.event.pageX + "px")
                       .style("top", d3.event.pageY - 80 + "px")
                       .style("display", "inline-block")
                       .html(fillTooltip(d.action_name,d.action_number_2,d.acres_affected_2));
               })
               .on("mouseover", function(d){
                 tooltip.transition().duration(200).style("opacity", 0.9);
               })
               .on("mouseout", function(d){
                 tooltip.transition().duration(500).style("opacity", 0);
               });

            //adding the second action number label to the barZone (if present)
           barZone.append("text")
               .attr("x", function (d) {
                   return xScale(d.action_number_2) + paddingLeft;
               })
               .attr("y", function (d,i) {
                   return yScale(d.action_name)+25;
               })
               .attr("dy", "1.1rem")
               .attr("dx", "-0.2rem")
               .attr("fill", "#fff")
               .attr("text-anchor", "end")
               .text(function (d) {
                   return d.action_number_2; //d.v21
               });

           svg.append("line")          // attach a line
               .style("stroke", "#000")
               .style("stroke-width", "0.5")
               .attr("x1", paddingLeft + 50)     // x position of the first end of the line
               .attr("y1", legendHeight)      // y position of the first end of the line
               .attr("x2", paddingLeft + 50)     // x position of the second end of the line
               .attr("y2", height+legendHeight + 15);   // y position of the second end of the line

         }

         //creating the tooltip when overing on a bar
         function fillTooltip(actName,actNum,acrAff){
             return '<span>Action: '+ actName + '</span><br/><span>Counter: ' + actNum + '<br/></span><span>Acres Affected: '+ acrAff.toFixed(2) + '<br/></span>'
         }

         //putting a legend to the right of the page
         function legendElement(container, color, label, xPos, id) {

           //once that the rectangle is drawn, it will never disappear
             container.append("rect")
                 .attr("class", "legend")
                 .attr("x", xPos)
                 .attr("height", 20)
                 .attr("y", 20)
                 .attr("width", 20)
                 .attr("fill", color);

             //every time I select an option I have to remove the legend text
             $("#legend_text" + id).remove();

             container.append("text")
                 .attr("class", "legend")
                 .attr("id", "legend_text" + id)
                 .attr("x", xPos + 25)
                 .attr("y", 20)
                 .attr("dy", "1.2rem")
                 .attr("fill", color)
                 .attr("text-anchor", "start")
                 .text(label);
         } // close legendElement

         function getActions(selectedElement, id){

           // first, I need to reset the values linked to the id chosen

           if (id == 1 ) {
             for (var i = 0; i < actions.length; i++) {
               actions[i].action_number_1 = 0;
               actions[i].acres_affected_1 = 0;
             }
           } else {
             for (var i = 0; i < actions.length; i++) {
               actions[i].action_number_2 = 0;
               actions[i].acres_affected_2 = 0;
             }
           }

           //presidents are shown by surname and then first and second name
           //in the database they are saved with first, second and then last name
           //getPresidentFromTable converts the selected element to something suitable for the database
           var result = getPresidentFromTable(selectedElement);

           if(result!=-1){ // -1 means that "Choose president or congress" was selected
            for (var i = 0; i < data.length; i++) {
              if (result == data[i].pres_or_congress) { // when i found the selected president/congress inside data
                for (var j = 0; j < actions.length; j++) { // I check all actions to find the same one the president did
                  var string = data[i].action;
                  var newString = string.replace(/(\w+).*/,"$1"); //replace the whole string with the first word
                  if (actions[j].action_name == newString) { // once I found it, I update the data inside actions[]
                    var acres;

                    if($.type(data[i].acres_affected)==="string" && data[i].acres_affected){
                      var temp = data[i].acres_affected.replace(',', '');
                      acres = parseFloat(temp);
                    } else {
                      acres = parseFloat(data[i].acres_affected) || 0;
                    }

                    if(id == 1){
                      actions[j].action_number_1++;
                      actions[j].acres_affected_1 += acres;
                    } else {
                      actions[j].action_number_2++;
                      actions[j].acres_affected_2 += acres;
                    }
                  }
                }
              }
            }
           } // close if
         } // close getActions

         function getPresidentFromTable(string){
           //parseInt called on a string checks the first character
           //isNaN checks if the given value is not a number
           //ex: 75th Congress -> 7 -> is a number, so I just return it because it's saved exactly as it is in the database
           //ex: Obama B. H. -> O -> is not a number
           if(isNaN(parseInt(string))){
             switch (string) {
               case "Unknown": //I called the empty string "Unknown"
                   return ""; //so the database wants ""
                 break;
               case "Choose a president or a congress": //this option is not related to the database
                   return -1; // I return -1
                 break;
               default:
                   // I check inside presidentsTable[] for the right match. Inside it there are
                   // - the select_name (i.e. the name shown by the user)
                   // - the table_name (i.e. the name used in the database)
                   for (var i = 0; i < presidentsTable.length; i++) {
                     if(string == presidentsTable[i].select_name){
                       return presidentsTable[i].table_name;
                     }
                   }
             }
           } else {
             return string;
           }
         } // close getPresidentFromTable

        function initializeActions(){
          // first, I search all actions from the database in actions[].action_name
          var found;
          for (var i = 0; i < data.length; i++) {
            found=false;
            var string = data[i].action;
            var newString = string.replace(/(\w+).*/,"$1"); //replace the whole string with the first word
            for (var a = 0; a<actions.length; a++){
              if(actions[a].action_name == newString){
                found = true;
                break;
              }
            }

            // if the action is not yet in actions[]
            //I add a new object to actions[] with that action and all its fields set to 0
            if(!found){
              actions.push({
                "action_name" : newString,
                "action_number_1" : 0,
                "acres_affected_1" : 0,
                "action_number_2" : 0,
                "acres_affected_2" : 0,
              });
            }
          }
        } //close initializeActions

        function orderPresidentsAndCongresses(){

          prescong.sort(function(a,b) {   //sorting result: 101st congress, 54th congress, presidents ordered by first name
            if (isNaN(a) || isNaN(b)) {
              return a > b ? 1 : -1;
            }
            return a - b;
          });

          prescong[0]="Unknown"; // I change "" to "Unknown" to make it clearer for the user

          j=0;
          var presidents=[];

          for (i = 0; i < prescong.length; i++) {
             // i pick up every string that is not a number (check made on the first character) ...
            if (isNaN(parseInt(prescong[i]))) {
              presidents[j] = prescong[i]; // ... and I save it in presidents[]
              j++;
            }
          }

          var itemToRemove = "Unknown"; // in presidents[] there are all presidents + "Unknown"
          presidents.splice($.inArray(itemToRemove, presidents),1); // I remove "Unknown" from the list

          var res=[]; // res will be an array of arrays

          for (var i = 0; i < presidents.length; i++) {
            //I save in presidentsTable the table name of the president, i.e. the name used in the database to identify him
            presidentsTable.push({
              "table_name" : presidents[i], // ex : "B. H. Obama"
              "select_name" : "" // ex : "Obama B. H." -> it will be added later
            });
            res[i] = presidents[i].split(" "); // res[0] = ["B."]["H."]["Obama"]   ,   res[1] = ["C."]["Coolidge"]    ,   ...
          }

          for (var i = 0; i < res.length; i++) {
            presidents[i] = res[i][res[i].length-1] + " " + res[i][0];
            // res[i][res[i].length-1] is the last element of the subarray. Ex: "Obama", "Coolidge", ...
            // res[i][0] is the first element of the subarray. Ex: "B.", "C."
            if(res[i].length == 3) { // if the current president has a second name ...
              presidents[i] += " " + res[i][1]; // ... I put it after the first name
            }
            presidentsTable[i].select_name = presidents[i]; // ex : "Obama B. H."
          } // presidents[0] = "Obama B. H."    ,     presidents[1] = "Coolidge C."

          presidents.sort(); // I order all presidents (now the sorting is made by surname)

          prescong.length -= presidents.length; // I remove all presidents from prescong as they were at the end of that array

          prescong.sort(function(a,b) {
            return parseInt(a) - parseInt(b); // I correctly sort all elements left inside prescong, i.e. all congresses
          });

          $.merge(prescong, presidents); // all congresses sorted + all presidents sorted = that's what will appear in the select
        } // close orderPresidentsAndCongresses()

    }); //close getJSON
}); // close function
