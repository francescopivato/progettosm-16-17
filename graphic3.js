$(document).ready(function(){


    $.getJSON("actions_under_antiquities_act.json", function(data, status){


      var j = 0;
      var parkAndInfoLength = 0;
      var states = [];
      var parks = [];
      var agency = [];
      var pres = [];
      var statesAndPark = {};
      var statesAndParkLength = 0;
      var parkAndInfo = {};

      var treeData = {
        "name":"States",
        "children" : new Array()
      };

      for(i=0; i<data.length; i++){
        if(states.indexOf(data[i].states)==-1){
          states[j] = data[i].states;
          j++;
        }
        var string = data[i].action;
        //replace the whole string with the first word
        var newString = string.replace(/(\w+).*/,"$1");
        if (newString == "Established") {
          parks[data[i].current_name] = "Established on " + (data[i].date).substring(0, (data[i].date).length-2) + data[i].year;
          pres[data[i].current_name] = "by " + data[i].pres_or_congress;
          agency[data[i].current_name] = "Agency: " + data[i].current_agency;

          parkAndInfo[parkAndInfoLength] = {"name" : data[i].current_name, "Original name" : data[i].original_name, "current agency" : data[i].current_agency,
                            "Established" : data[i].date, "by" : data[i].pres_or_congress};
          parkAndInfoLength++;
        }
      }

      states.sort();
      // console.log(parks);

      for(i=0; i<states.length; i++) {
        statesAndPark[statesAndParkLength] = {"nameState" : states[i], "parks" : new Array()};
        statesAndParkLength++;
      }



      for(i=0; i<states.length; i++){
        var Parks = [];
        for(j=0; j<data.length; j++){
          if(data[j].states == states[i]) {
            if (Parks.indexOf(data[j].current_name) == -1) {
              Parks.push(data[j].current_name);
            }
          }
        }
        statesAndPark[i].parks = Parks;
        statesAndPark[i].parks.sort();
      }

      //console.log(parkAndInfo);

      for(i=0; i<states.length; i++){
        treeData.children[i] = {"name" : states[i], "children" : new Array()};
        for(j=0; j<statesAndPark[i].parks.length; j++) {
          treeData.children[i].children[j] = {"name" : statesAndPark[i].parks[j],
          "children" : [ {"name" : parks[statesAndPark[i].parks[j]]}, {"name" : pres[statesAndPark[i].parks[j]]}, {"name" : agency[statesAndPark[i].parks[j]]} ]};
        /* { parks[statesAndPark[i].parks[j]], pres[statesAndPark[i].parks[j]], agency[statesAndPark[i].parks[j]]} };*/

        }
      }

      // Set the dimensions and margins of the diagram
      var margin = {top: 20, right: 10, bottom: 30, left: 30},
          width = 1500 - margin.left - margin.right,
          height = 2000 - margin.top - margin.bottom;

      // append the svg object to the body of the page
      // appends a 'group' element to 'svg'
      // moves the 'group' element to the top left margin
      var svg = d3.select("#section3").append("svg")
          .attr("width", width + margin.right + margin.left)
          .attr("height", height + margin.top + margin.bottom)
          .append("g")
          .attr("transform", "translate("
                + margin.left + "," + margin.top + ")");

      var i = 0,
          duration = 750,
          root;

      // declares a tree layout and assigns the size
      var treemap = d3.tree().size([height, width]);

      // Assigns parent, children, height, depth
      root = d3.hierarchy(treeData, function(d) { return d.children; });
      root.x0 = height / 2;
      root.y0 = 0;

      // Collapse after the second level
      root.children.forEach(collapse);

      update(root);

      // Collapse the node and all it's children
      function collapse(d) {
        if(d.children) {
          d._children = d.children
          d._children.forEach(collapse)
          d.children = null
        }
      }

      function update(source) {

        // Assigns the x and y position for the nodes
        var treeData = treemap(root);

        // Compute the new tree layout.
        var nodes = treeData.descendants(),
            links = treeData.descendants().slice(1);

        // Normalize for fixed-depth.
        nodes.forEach(function(d){ d.y = d.depth * 250});

        // ****************** Nodes section ***************************

        // Update the nodes...
        var node = svg.selectAll('g.node')
            .data(nodes, function(d) {return d.id || (d.id = ++i); });

        // Enter any new modes at the parent's previous position.
        var nodeEnter = node.enter().append('g')
            .attr('class', 'node')
            .attr("transform", function(d) {
              return "translate(" + (source.y0 + margin.top) + "," + (source.x0 + margin.left) + ")";
          })
          .on('click', click);

        // Add Circle for the nodes
        nodeEnter.append('circle')
            .attr('class', 'node')
            .attr('r', 1e-6)
            .style("fill", function(d) {
                return d._children ? "lightsteelblue" : "#fff";
            })

        // Add labels for the nodes
        nodeEnter.append('text')
            .attr("dy", ".35em")
            .attr("x", function(d) {
                return d.children || d._children ? 0 : 13;
            })
            .attr('y', function(d) {
			        	return d.children || d._children ? -margin.top : 0;
			      })
            .attr("text-anchor", function(d) {
                return d.children || d._children ? "middle" : "start";
            })
            .style("font-weight", "bold")
            .text(function(d) { return d.data.name; });

        // add number of children to node circle
        nodeEnter.append('text')
	        .attr('x', function(d) {
                        if (d.children) return d.children.length > 9 ? -6 : -3;
                        else if(d._children) return d._children.length > 9 ? -6 : -3 })
	        .attr('y', 3)
	        .attr('cursor', 'pointer')
	        .style('font-size', '10px')
	        .text(function(d) {
	        	if (d.children) return d.children.length;
	        	else if (d._children) return d._children.length;
	        });

        // UPDATE
        var nodeUpdate = nodeEnter.merge(node);

        // Transition to the proper position for the node
        nodeUpdate.transition()
          .duration(duration)
          .attr("transform", function(d) {
              return "translate(" + (d.y+margin.top) + "," + (d.x + margin.left) + ")";
           });

        // Update the node attributes and style
        nodeUpdate.select('circle.node')
          .attr('r', 10)
          .style("fill", function(d) {
              return d._children ? "lightsteelblue" : "#fff";
          })
          .attr('cursor', 'pointer');


        // Remove any exiting nodes
        var nodeExit = node.exit().transition()
            .duration(duration)
            .attr("transform", function(d) {
                return "translate(" + (source.y + margin.top) + "," + (source.x + margin.left) + ")";
            })
            .remove();

        // On exit reduce the node circles size to 0
        nodeExit.select('circle')
          .attr('r', 1e-6);

        // On exit reduce the opacity of text labels
        nodeExit.select('text')
          .style('fill-opacity', 1e-6);

        // ****************** links section ***************************

        // Update the links...
        var link = svg.selectAll('path.link')
            .data(links, function(d) { return d.id; });

        // Enter any new links at the parent's previous position.
        var linkEnter = link.enter().insert('path', "g")
            .attr("class", "link")
            .attr('d', function(d){
              var o = {x: source.x0 + margin.left, y: source.y0 + margin.top}
              return diagonal(o, o)
            });

        // UPDATE
        var linkUpdate = linkEnter.merge(link);

        // Transition back to the parent element position
        linkUpdate.transition()
            .duration(duration)
            .attr('d', function(d){ return diagonal(d, d.parent) });

        // Remove any exiting links
        var linkExit = link.exit().transition()
            .duration(duration)
            .attr('d', function(d) {
              var o = {x: source.x, y: source.y}
              return diagonal(o, o)
            })
            .remove();

        // Store the old positions for transition.
        nodes.forEach(function(d){
          d.x0 = d.x + margin.left;
          d.y0 = d.y + margin.top;
        });

        // Creates a curved (diagonal) path from parent to the child nodes
        function diagonal(s, d) {

          // path = `M ${s.y + margin.top} ${s.x + margin.left}
          //         C ${((s.y + d.y + (margin.top * 2)) / 2)} ${s.x + margin.left},
          //           ${((s.y + d.y + (margin.top * 2)) / 2)} ${d.x + margin.left},
          //           ${d.y + margin.top} ${d.x + margin.left}`

            path = 'M ' + (s.y + margin.top) + ' ' + (s.x + margin.left) +
  					        'C ' + ((s.y + d.y + (margin.top * 2)) / 2) + ' ' + (s.x + margin.left) +
  					        ', ' + ((s.y + d.y + (margin.top * 2)) / 2) + ' ' + (d.x + margin.left) +
  					        ', ' + (d.y + margin.top) + ' ' + (d.x + margin.left);

          return path
        }

        // Toggle children on click.
        function click(d) {
          if (d.children) {
              d._children = d.children;
              d.children = null;
            } else {
              d.children = d._children;
              d._children = null;
            }
          update(d);
        }
      }


    });

});
