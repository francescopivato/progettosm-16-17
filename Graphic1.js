$(document).ready(function(){

    $.getJSON("actions_under_antiquities_act.json", function(data, status){

    var years = [],
    actions = [],
    i,
    j = 0,
    condition=false,
    found=false,
    width=960,
    height=500,
    radius = Math.min(width, height) / 2,
    innerRadius=0;
    legendRectSize=18,
    legendSpacing=4;

    var color = d3.scaleOrdinal(d3.schemeCategory20);

    var svg = d3.select("#section1")
                                .append("svg")
                                .attr("width", width)
                                .attr("height", height)
                                .append("g")
                                .attr("id", "main_g")
                                .attr("transform", "translate(" + width/2 + "," + height/2 + ")");

    var tooltip = d3.select("#chart").append('div')
    .attr('class', 'tooltip')
    .style("opacity", 0);

    tooltip.append('div')
      .attr('class', 'action_label');

    tooltip.append('div')
      .attr('class', 'count');

    tooltip.append('div')
      .attr('class', 'percent');

    tooltip.append('div')
      .attr("class", "acres");

    var arc = d3.arc()
                      .outerRadius(radius)
                      .innerRadius(innerRadius);

    //every action is put as an object inside actions[]
    insertAllActions();

    // when I select Pie Chart, it is immediately displayed a pie with all actions
    makePie();

    //collecting all years listed in data, inside years[]
    //some years are not specified. An empty field is written instead -> ""
    for(i=0; i<data.length; i++){
      if($.inArray(data[i].year, years)==-1){
        years[j] = data[i].year;
        j++;
      }
    }
    years.sort(); //sorts all the years from the eldest to the newest
    years[0] = "0 Unknown year"; // Replace the empty string "" with a string more understandable
    //the zero at the beginning is needed when parsing to int the string, either it won't work
    years[years.length] = "All years"; // I create a new last element, which is all years

    //adding to the HTML page all the years as an <option> in a <select>
    for (var i = 0; i < years.length; i++){
          $("#select_g1").append("<option value=\"\">" + years[i] + "</option>");
    }

    //Check if year chosen is tracked by jQuery adding a triggerHandler event to <select>
    // trigger: it runs since the default action the element
    //triggerHandler: it avoids the default action of the element

    $("#select_g1").select(function(){
      //the if is necessary because everytime I click on the select it shows up the value
      //every value would be displayed twice if it wasn't for the if
      if (condition == false){
        condition = true;
        actions = [];
      } else {
        for (var i=0; i<data.length; i++){
          //if I found a match between the year selected and the years in the data ...
          if(parseInt($("#select_g1 option:selected").text()) == data[i].year){
            //... I insert the action made in that year inside actions[]
            insertActions(i);
        } // closing if
      } // closing for
      if($("#select_g1 option:selected").text() == "All years"){
        insertAllActions();
      } // closing if
      condition=false;
      makePie();
    }
    });

    $("#select_g1").click(function(){
      $("#select_g1").triggerHandler("select");
    });

    function insertActions(index){
      found=false;
      var acres;

      if($.type(data[index].acres_affected)==="string" && data[index].acres_affected){
        var temp = data[index].acres_affected.replace(',', '');
        acres = parseFloat(temp);
      } else {
        acres = parseFloat(data[index].acres_affected) || 0;
      }

      var string = data[index].action;
      //replace the whole string with the first word
      var newString = string.replace(/(\w+).*/,"$1");
      for (var a = 0; a<actions.length; a++){
        if(actions[a].action == newString){
          found = true;
          index = a;
          break;
        }
      }

      if(found){
        actions[index].counter++;
        actions[index].acres_affected += acres;
      } else {
        actions.push({
          "action" : newString,
          "counter" : 1,
          "acres_affected" : acres
        });
      }
    } //close function insertActions

    function insertAllActions(){
      for(i=0; i<data.length; i++){
        insertActions(i);
      }
    }

    function makePie(){

      $("#main_g").empty();

      var pie = d3.pie()
                        .value(function(d) {return d.counter; })
                        .sort(null);

      var path = svg.selectAll(".path")
                                      .data(pie(actions))
                                      .enter()
                                      .append("path")
                                      .attr("class", "path")
                                      .attr("d", arc)
                                      .attr("fill", function (d,i){return color(d.data.action);
                                      });

      path.on('mouseover', function(d) {
          var total = d3.sum(actions.map(function(d) {
          return d.counter;
          }));
        var percent = Math.round(1000 * d.data.counter / total) / 10;
        tooltip.select('.action_label').html("Action: " + checkAction( d.data.action));
        tooltip.select('.count').html("Counter: " + d.data.counter);
        tooltip.select('.percent').html("Percent: " + percent + '%');
        //round decimals to the first two
        tooltip.select('.acres').html("Acres Affected: " + d.data.acres_affected.toFixed(2));
        tooltip.transition().duration(200).style("opacity", 0.9);
      });

      path.on('mouseout', function() {
        tooltip.transition().duration(500).style("opacity", 0);
      });

      path.on('mousemove', function(d) {
        window.isMobile = /iphone|ipod|ipad|android|blackberry|opera mini|opera mobi|skyfire|maemo|windows phone|palm|iemobile|symbian|symbianos|fennec/i.test(navigator.userAgent.toLowerCase());
        if(!window.isMobile){
          tooltip.style('top', (d3.event.layerY - radius + 'px'))
                  .style('left', (d3.event.layerX + 'px'));
        } else {
          tooltip.style('top', (d3.event.layerY - radius*2 + 'px'))
                  .style('left', (d3.event.layerX + 'px'));
        }
      });

      var legend = svg.selectAll(".legend")
                                          .data(color.domain())
                                          .enter()
                                          .append("g")
                                          .attr("class", "legend")
                                          .attr("transform", function(d,i){
                                            var height = legendRectSize+legendSpacing;
                                            var offset = height * color.domain().length/2;
                                            var horz = radius + legendSpacing*10;
                                            var vert = i*height - offset;
                                            return "translate(" + horz + "," + vert + ")";
                                          });

      legend.append("rect")
                          .attr("width", legendRectSize)
                          .attr("height", legendRectSize)
                          .style("fill", color)
                          .style("fill", color);

      legend.append("text")
                          .attr("x", legendRectSize+legendSpacing)
                          .attr("y", legendRectSize-legendSpacing)
                          .text(function (d) {
                            return checkAction(d);
                          });
    } // close function makePie

    function checkAction(string){
      switch (string) {
        case "Boundaries":
          string="Boundaries confirmed";
          break;
        case "Restrictions":
          string="Restrictions modified";
          break;
        case "Amending":
          string="Amending description";
          break;
        case "Partially":
          string="Partially redesigned";
          break;
        default:
      }
      return string;
    }

  }); // close getJSON
}); //close function
